const Router = require("express");
const fileRouter = new Router()
const authMiddleware = require('../middleware/authMiddleware')

const {
  createDirController,
  getFilesController,
  uploadFileController,
  downloadFileController,
  deleteFileController
} =  require('../controllers/fileController');

fileRouter.post('', authMiddleware, createDirController)
fileRouter.get('', authMiddleware, getFilesController)
fileRouter.post('/upload', authMiddleware, uploadFileController)
fileRouter.get('/download', authMiddleware, downloadFileController)
fileRouter.delete('/', authMiddleware, deleteFileController)

module.exports = fileRouter

