const Router = require("express");
const {
  registration,
  login
} = require('../controllers/authController');

const authRouter = new Router();

authRouter.post('/registration', registration)
authRouter.post('/login', login)


module.exports = authRouter;