const express = require("express");
const app = express();
const authRouter = require('./routes/authRouter');
const fileRouter = require('./routes/fileRouter');
const cors = require('./middleware/corsMidelware')
const fileUpload = require("express-fileupload")
app.use(express.json());
app.use(cors);
app.use(fileUpload({}))
app.use("/api/auth", authRouter);
app.use("/api/files", fileRouter)

module.exports = app;