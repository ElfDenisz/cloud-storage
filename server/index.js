const http = require('http');
require('dotenv').config();
const app = require('./app');
const { mongoConnect } = require('./services/mongo');

const server = http.createServer(app);
const PORT = process.env.PORT || 8000;
const startServer = async () => {
 try {
    await mongoConnect();
    server.listen(PORT, () => {
      console.log('Server started on port ', PORT);
    })
  }
  catch (e) {
    console.log(e);
  }
}

startServer();