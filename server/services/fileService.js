const fs = require('fs')
const File = require('../models/File')


async function createDir(file) {
    const filePath = `files\\${file.user}\\${file.path}`
    return new Promise(((resolve, reject) => {
        try {
            if (!fs.existsSync(filePath)) {
                console.log(filePath)
                fs.mkdirSync(filePath)
                return resolve({ message: 'File was created' })
            } else {
                return reject({ message: "File already exist" })
            }
        } catch (e) {
            return reject({ message: 'File error' })
        }
    }))
}
function getPath(file) {
    return 'files'+ '\\' + file.user + '\\' + file.path
}
function deleteFile(file) {
    const path = getPath(file)
    if (file.type === 'dir') {
        fs.rmdirSync(path)
    } else {
        fs.unlinkSync(path)
    }
}
module.exports = { createDir, getPath, deleteFile };