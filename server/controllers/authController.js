
const User = require("../models/User");
const bcrypt = require("bcryptjs");
const { check, validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");
require('dotenv').config();
const {createDir} = require('../services/fileService')
const File = require('../models/File')


async function registration(req, res) {

    check('email', "Uncorrect email").isEmail(),
        check('password', 'Password must be longer than 3 and shorter than 12').isLength({ min: 3, max: 12 })

    try {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            console.log("Uncorrect request")
            return res.status(400).json({ message: "Uncorrect request", errors })
        }
        const { email, password } = req.body
        const candidate = await User.findOne({ email })
        if (candidate) {
            return res.status(400).json({ message: `User with email ${email} already exist` })
        }
        const hashPassword = await bcrypt.hash(password, 8)
        const user = new User({ email, password: hashPassword })
        await user.save()
        console.log(createDir)
        await createDir(new File({user:user.id, name: ''}))
        res.json({ message: "User was created" })
    } catch (e) {
        console.log(e)
        res.send({ message: "Server error" })

    }

}
async function login(req, res) {
    const secretKey = process.env.secretKey;
    try {
        const { email, password } = req.body
        const user = await User.findOne({ email })
        if (!user) {
            return res.status(404).json({ message: "User not found" })
        }
        const isPassValid = bcrypt.compareSync(password, user.password)
        if (!isPassValid) {
            return res.status(400).json({ message: "Invalid password" })
        }
        const token = jwt.sign({ id: user.id }, secretKey, { expiresIn: "1h" })
        return res.json({
            token,
            user: {
                id: user.id,
                email: user.email,
                diskSpace: user.diskSpace,
                usedSpace: user.usedSpace,
                avatar: user.avatar
            }
        })
    } catch (e) {
        console.log(e)
        res.send({ message: "Server error" })
    }
}

module.exports = { registration, login };