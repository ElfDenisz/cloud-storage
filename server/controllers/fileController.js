const {createDir, getPath, deleteFile} = require('../services/fileService')
const fs = require('fs')
const User = require('../models/User')
const File = require('../models/File')



async function createDirController(req, res) {
    try {
        const {name, type, parent} = req.body
            const file = new File({name, type, parent, user: req.user.id})
            const parentFile = await File.findOne({_id: parent})

      
        if (!parentFile) {
            file.path = name
            console.log(createDir)
            await createDir(file);
        } else {
            file.path = `${parentFile.path}\\${file.name}`
            await createDir(file)
            parentFile.childs.push(file._id)
            await parentFile.save()
        }
        await file.save()
        return res.json(file)
    } catch (e) {
        console.log(e)
        return res.status(400).json(e)
    }
}
async function getFilesController(req, res) {
    try {

        const files = await File.find({ user: req.user.id, parent: req.query.parent }).sort({ name: 1 })
        return res.json(files)
    }

    catch (e) {
        console.log(e)
        return res.status(500).json({ message: "Can not get files" })
    }
}
async function uploadFileController(req, res) {

    console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!')
    try {
       let myfilePath = "files"
       const file = req.files.file
        console.log(myfilePath)

        const parent = await File.findOne({user: req.user.id, _id: req.body.parent})
        const user = await User.findOne({_id: req.user.id})

        if (user.usedSpace + file.size > user.diskSpace) {
            return res.status(400).json({message: 'There no space on the disk'})
        }

        user.usedSpace = user.usedSpace + file.size

        let path;
        if (parent) {
            console.log(myfilePath)
            path = `${myfilePath}\\${user._id}\\${parent.path}\\${file.name}`
            console.log(path)
        } else {
            console.log(myfilePath)
            path = `${myfilePath}\\${user._id}\\${file.name}`
            console.log(path)
        }

        if (fs.existsSync(path)) {
            return res.status(400).json({message: 'File already exist'})
        }
        file.mv(path)

        const type = file.name.split('.').pop()
        let filePath = file.name
        if (parent) {
            filePath = parent.path + "\\" + file.name
        }
        const dbFile = new File({
            name: file.name,
            type,
            size: file.size,
            path: filePath,
            parent: parent?._id,
            user: user._id
        });

        await dbFile.save()
        await user.save()

        res.json(dbFile)
    } catch (e) {
        console.log(e)
        return res.status(500).json({message: "Upload error"})
    }
}

async function downloadFileController(req, res) {
    try {
        console.log('downloadFileController')
        const file = await File.findOne({_id: req.query.id, user: req.user.id})
        const path = getPath(file)
        console.log(path)
        if (fs.existsSync(path)) {
            return res.download(path, file.name)
        }
        return res.status(400).json({message: "Download error"})
    } catch (e) {
        console.log(e)
        res.status(500).json({message: "Download error"})
    }
}
async function deleteFileController(req, res) {
    try {
        const file = await File.findOne({_id: req.query.id, user: req.user.id})
        if (!file) {
            return res.status(400).json({message: 'file not found'})
        }
        deleteFile(file)
        await file.remove()
        return res.json({message: 'File was deleted'})
    } catch (e) {
        console.log(e)
        return res.status(400).json({message: 'Dir is not empty'})
    }
}

module.exports = {
    createDirController,
    getFilesController,
    uploadFileController,
    downloadFileController,
    deleteFileController

  };
