import Navbar from "./components/navbar/Navbar";
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import Registration from "./components/registration/Registration";
import Login from "./components/registration/Login";
import { useDispatch, useSelector } from "react-redux";
import Disk from "./components/disk/Disk";


function App() {
  const isAuth = useSelector(state => state.user.isAuth)
  return (
    <div>
      <BrowserRouter>
        <Navbar />
        {!isAuth ?
          <Switch>
            <Route path="/registration" component={Registration} />
            <Route path="/login" component={Login} />
            <Redirect to='/login' />
          </Switch>
          :
          <Switch>
            <Route exact path="/" component={Disk} />
     <Redirect to="/" />
          </Switch>
        }
      </BrowserRouter>

    </div>
  );
}

export default App;
