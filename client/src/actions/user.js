
import axios from 'axios'
import { setUser } from '../reducers/userReducer'
const API_URL = 'http://localhost:3001'
export const registration = async (email, password) => {
  try {
     const response = await axios.post(`${API_URL}/api/auth/registration`, {
          email,
          password
      })
      alert(response.data.message)
  } catch (e) {
      alert(e.response.data.message)
  }
}
export const login =  (email, password) => {
    return async dispatch => {
        try {
            console.log('login')
            const response = await axios.post(`${API_URL}/api/auth/login`, {
                email,
                password
            })
            console.log(response.data)
            dispatch(setUser(response.data.user))
            localStorage.setItem('token', response.data.token)
        } catch (e) {
            alert(e.response.data.message)
        }
    }
}