import axios from 'axios'

import { addFile, deleteFileAction, setFiles } from "../reducers/fileReducer";
const API_URL = 'http://localhost:3001'
export function getFiles(dirId) {


  return async dispatch => {
    try {
      let url = `${API_URL}/api/files`
      if (dirId) {
        url = `${API_URL}/api/files?parent=${dirId}`
      }
      console.log(url)
      const response = await axios.get(url, {
        headers: { Authorization: `Bearer ${localStorage.getItem('token')}` }
      });
      dispatch(setFiles(response.data))
    } catch (e) {
      alert(e.response.data.message)
    }
  }
}
export function createDir(dirId, name) {

  return async dispatch => {
    try {
      const response = await axios.post(`${API_URL}/api/files`, {
        name,
        parent: dirId,
        type: 'dir'
      }, {
        headers: { Authorization: `Bearer ${localStorage.getItem('token')}` }
      })
      console.log(API_URL)
      dispatch(addFile(response.data))
    } catch (e) {
      alert(e.response.data)
    }
  }
}
export function uploadFile(file, dirId) {

  return async dispatch => {
    try {
      const formData = new FormData()

      formData.append('file', file)
      if (dirId) {
        formData.append('parent', dirId)
      }
      for (var value of formData.values()) {
        console.log(value);
      }
      const uploadFile = { name: file.name, progress: 0, id: Date.now() }
      const response = await axios.post(`${API_URL}/api/files/upload`, formData, {
        headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
        onUploadProgress: progressEvent => {
          const totalLength = progressEvent.lengthComputable ? progressEvent.total : progressEvent.target.getResponseHeader('content-length') || progressEvent.target.getResponseHeader('x-decompressed-content-length');
          console.log(totalLength)
          if (totalLength) {
            let progress = Math.round((progressEvent.loaded * 100) / totalLength)

          }
        }

      });
      dispatch(addFile(response.data))
    } catch (e) {
      alert(e.response)
    }
  }
}
export async function downloadFile(file) {
  const response = await fetch(`${API_URL}/api/files/download?id=${file._id}`,{
      headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
      }
  })
  if (response.status === 200) {
      const blob = await response.blob()
      const downloadUrl = window.URL.createObjectURL(blob)
      const link = document.createElement('a')
      link.href = downloadUrl
      link.download = file.name
      document.body.appendChild(link)
      link.click()
      link.remove()
  }
}

export function deleteFile(file) {
  return async dispatch => {
      try {
          const response = await axios.delete(`${API_URL}/api/files?id=${file._id}`,{
              headers:{
                  Authorization: `Bearer ${localStorage.getItem('token')}`
              }
          })
          dispatch(deleteFileAction(file._id))
          alert(response.data.message)
      } catch (e) {
          alert(e?.response?.data?.message)
      }
  }
}