import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { createDir, getFiles, uploadFile } from "../../actions/file";
import FileList from './FileList';
import './disc.css'
import Popup from './Popup';
import {setCurrentDir, setFileView, setPopupDisplay} from "../../reducers/fileReducer";


function Disk() {
  const currentDir = useSelector(state => state.files.currentDir)
  const dirStack = useSelector(state => state.files.dirStack)


  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(getFiles(currentDir))

  }, [currentDir])
  function createDirHandler() {
    dispatch(createDir(currentDir, 'qqqqqqqqqq'))
}
function showPopupHandler() {
  dispatch(setPopupDisplay('flex'))
}
function backClickHandler() {
  const backDirId = dirStack.pop()
  dispatch(setCurrentDir(backDirId))
}
function fileUploadHandler(event) {
  const files = [...event.target.files]
  console.log(files)
  files.forEach(file => dispatch(uploadFile(file, currentDir)))
}

  return (
    <div className="disk">
      <div className="disk__btns">
        <button className="disk__back"  onClick={() => backClickHandler()}>Go Back</button>
        <button className="disk__create" onClick={() => showPopupHandler()}>Create Folder</button>
        <div className="disk__upload">
                        <label htmlFor="disk__upload-input" className="disk__upload-label">Download file</label>
                        <input multiple={true} onChange={(event)=> fileUploadHandler(event)} type="file" id="disk__upload-input" className="disk__upload-input"/>
                    </div>
      </div>
      <FileList />
      <Popup/>
    </div>
  )
}

export default Disk