import React from 'react'
import './filelist.css'
import { useSelector } from "react-redux";
import SingleFile from './File';

function FileList() {
  const files = useSelector(state => state.files.files)
  if (files.length == 0) {
    return (
      <div className='loader'>Files not found</div>
    )
  }
  return (
    <div className='filelist'>
      <div className="filelist__header">
        <div className="filelist__name">Name</div>
        <div className="filelist__date">Date</div>
        <div className="filelist__size">Size</div>
      </div>
      <div className='fileplate'>
        {files.map(file =>
          <SingleFile key={file._id} file={file} />
        )}
      </div>

    </div>
  )
}

export default FileList