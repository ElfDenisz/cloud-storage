import React, { useState } from 'react'
import './navbar.css'
import Logo from '../../assets/img/logo.png'
import {NavLink} from "react-router-dom";


function Navbar() {
  return (
    <div className="navbar">
    <div className="container">
        <img src={Logo} alt="" className="navbar__logo" />
        <div className="navbar__header">CLOUD</div>
        <div className="navbar__login"><NavLink to="/login">login</NavLink></div>
        <div className="navbar__registration"><NavLink to="/registration">registration</NavLink></div>
        <div className="navbar__login">logout</div>
    </div>
</div>
  )
}

export default Navbar